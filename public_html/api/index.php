<?php
require 'vendor/autoload.php';
header("Content-Type: application/json");

use liaml\pstr\Models\Paste;

$uri = $_SERVER['REQUEST_URI'];

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (preg_match("#^\/api\/pastes\/\w+$#", $uri)) {
        $_id = explode("/", $uri)[3];
        $paste = new Paste();
        $paste->read($_id);
    }
} else {
    if (preg_match("#^\/api\/pastes$#", $uri)) {
        $paste = new Paste($_POST['title'], $_POST['body']);
        $paste->create();
    }
}
