<?php
namespace liaml\pstr\Models;

use liaml\pstr\Core\Database;

class Paste
{
    private $client = null;

    private $title = "";
    private $body = "";

    public function __construct($title = "", $body = "")
    {
        $this->client = (new Database())->getClient();

        $this->title = $title;
        $this->body = $body;
    }

    public function create()
    {
        $collection = $this->client->pstr->pastes;

        $pasteToInsert = ['title' => $this->title, 'body' => $this->body];

        $result = $collection->insertOne($pasteToInsert);

        echo json_encode(["_id" => (string) $result->getInsertedId()]);
    }

    public function read($_id)
    {
        $collection = $this->client->pstr->pastes;

        try {
            $_id = new \MongoDB\BSON\ObjectId($_id);
        } catch (\Exception$e) {}

        $result = $collection->findOne(["_id" => $_id]);

        echo json_encode($result);
    }
}
