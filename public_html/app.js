document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/pastes");
    xhr.onloadend = () => {
        window.location.href = "/p/" + JSON.parse(xhr.responseText)._id;
    }
    xhr.send(new FormData(e.target))
});

let urlSplit = window.location.pathname.split("/");

if (urlSplit[1] == "p") {

    document.querySelector("form").remove();
    document.querySelector(".paste").style.display = "block";

    let id = urlSplit[2];
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/pastes/" + id);
    xhr.onloadend = () => {
        let paste = JSON.parse(xhr.responseText);
        title.textContent = paste.title;
        body.textContent = paste.body;
    }
    xhr.send()
}

let copyBtn = document.querySelector('#copy');
copyBtn.addEventListener('click', function (event) {
    let copyTextarea = document.querySelector('textarea');
    copyTextarea.disabled = false;
    copyTextarea.focus();
    copyTextarea.select();
    try {
        let successful = document.execCommand('copy');
        if (successful) {
            copied.className = "fade";
            setTimeout(() => { copied.className = ""; }, 2000);
        }
    } catch (err) {
        alert('Unable to copy');
    }
    setTimeout(function () {
        copyTextarea.selectionStart = 0;
        copyTextarea.selectionEnd = 0;
    }, 0);
    copyTextarea.disabled = true;
});